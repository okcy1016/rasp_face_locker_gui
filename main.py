import time, cv2, requests, json, argparse, sys, os, threading, mainui, pinui
from PyQt5.QtWidgets import QApplication, QMainWindow
from PyQt5.QtCore import QTimer, QEventLoop
from PyQt5.QtGui import QImage, QPixmap
from pynfc import Nfc, Desfire, Timeout
from gpiozero import LED


class main_window(QMainWindow):
    def __init__(self, args):
        super(main_window, self).__init__()
        # init variables
        self.passcode = "123456"
        self.face_detected = False
        self.face_detected_count = 0
        self.pause_face_detect_flag = False
        self.face_identified_success = False
        self.nfc_identified_success = False
        self.pin_identified_success = False
        self.door_unlocked = False
        # setup interface from Designer
        self.ui = mainui.Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.pushButton_1.clicked.connect(self.authenticate_with_pin)
        # setup pin authenticate window
        self.pin_auth_window = pin_auth_window(self)
        ## init opencv
        self.camera_ins = cv2.VideoCapture(0)
        self.cv2_face_cascade = cv2.CascadeClassifier("opencv_face_detect_model/haarcascade_frontalface_default.xml")
        self.locker_control_gpio = LED(21)
        self.init_timer()
        # init nfc detect thread
        threading.Thread(target=self.nfc_unlock).start()
        self.show()

    def init_timer(self):
        self.capture_image_timer = QTimer()
        self.capture_image_timer.timeout.connect(self.capture_image)
        self.capture_image_timer.start(50)
        # delay for initializing camera
        tmp_qeventloop = QEventLoop()
        QTimer.singleShot(3000, tmp_qeventloop.quit)
        tmp_qeventloop.exec_()
        self.image_refresh_timer = QTimer()
        self.image_refresh_timer.timeout.connect(self.refresh_camera_show)
        self.image_refresh_timer.start(50)
        self.face_detect_timer = QTimer()
        self.face_detect_timer.timeout.connect(self.detect_face)
        self.face_detect_timer.start(200)
        self.led_control_timer = QTimer()
        self.led_control_timer.timeout.connect(self.set_led_and_gpio_status)
        self.led_control_timer.start(200)
        self.recognize_face_timer = QTimer()
        self.recognize_face_timer.timeout.connect(self.recognize_face_with_api)
        self.recognize_face_timer.start(200)
        self.door_control_timer = QTimer()
        self.door_control_timer.timeout.connect(self.door_control)
        self.door_control_timer.start(200)

    def capture_image(self):
        _, BGR_captured_image = self.camera_ins.read()
        self.zoomed_out_BGR_captured_image = cv2.resize(BGR_captured_image, (320, 240))
        zoomed_out_captured_image = cv2.cvtColor(self.zoomed_out_BGR_captured_image, cv2.COLOR_BGR2RGB)
        self.qImage0 = QImage(zoomed_out_captured_image.data, zoomed_out_captured_image.shape[1], zoomed_out_captured_image.shape[0], QImage.Format_RGB888)

    def detect_face(self):
        zoomed_out_GRAY_captured_image = cv2.cvtColor(self.zoomed_out_BGR_captured_image, cv2.COLOR_BGR2GRAY)
        face_detect_result = self.cv2_face_cascade.detectMultiScale(zoomed_out_GRAY_captured_image, 1.3, 5)
        # eliminate accidental errors
        if not (type(face_detect_result) == tuple and face_detect_result == ()):
            self.face_detected_count += 1
        else:
            self.face_detected_count -= 1
            if self.face_detected_count < 0:
                self.face_detected_count = 0
        if self.face_detected_count > 3:
            self.face_detected_count = 0
            self.face_detected = True
            # delay without freezing the window
            tmp_qeventloop = QEventLoop()
            QTimer.singleShot(2000, tmp_qeventloop.quit)
            tmp_qeventloop.exec_()
            self.face_detected = False
        else:
            self.face_detected = False

    def set_led_and_gpio_status(self):
        if self.face_detected:
            self.ui.faceDetectedLabel.setPixmap(QPixmap("assets/green-led-on.png"))
        else:
            self.ui.faceDetectedLabel.setPixmap(QPixmap("assets/red-led-on.png"))
        if self.face_identified_success:
            self.ui.faceDetectedLabel.setPixmap(QPixmap("assets/green-led-on.png"))
            self.ui.faceIdentifiedLabel.setPixmap(QPixmap("assets/green-led-on.png"))
        else:
            self.ui.faceIdentifiedLabel.setPixmap(QPixmap("assets/red-led-on.png"))
        if self.nfc_identified_success:
            self.ui.nfcLedLabel.setPixmap(QPixmap("assets/green-led-on.png"))
        else:
            self.ui.nfcLedLabel.setPixmap(QPixmap("assets/red-led-on.png"))
        if self.door_unlocked:
            self.ui.doorOpenedLabel.setPixmap(QPixmap("assets/green-led-on.png"))
            self.locker_control_gpio.on()
        else:
            self.ui.doorOpenedLabel.setPixmap(QPixmap("assets/red-led-on.png"))
            self.locker_control_gpio.off()
            
    def refresh_camera_show(self):
        self.ui.cameraViewLabel.setPixmap(QPixmap.fromImage(self.qImage0))

    def recognize_face_with_api(self):
        if self.face_detected:
            # pause recognization timer when recognize
            self.recognize_face_timer.stop()
            # pause face detect when recognize face
            self.face_detect_timer.stop()
            face_compare_api_url = "https://api-cn.faceplusplus.com/facepp/v3/search"
            data_dict = {
                "api_key": "DojIacYLsNU2ivBxC8bA0IQVYDfOSfpl",
                "api_secret": "DBVC3sFlXMTljLbVS8Z3zigczYmfDSuC",
                "faceset_token": "bf9c1377b9e184f2bb4efbd966872f9f"
            }
            # save image to local from camera
            if not os.path.isdir("tmp"):
                os.mkdir("tmp")
            cv2.imwrite("tmp/captured_face.png", self.zoomed_out_BGR_captured_image)
            image_file_to_compare = {"image_file": open("tmp/captured_face.png", "rb")}
            http_response = requests.post(face_compare_api_url, data=data_dict, files=image_file_to_compare)
            response_dict = json.loads(http_response.text)
            if args.debug:
                print(response_dict)
            if response_dict["faces"] != []:
                for result in response_dict["results"]:
                    print("Confidence: ", result["confidence"])
                    if result["confidence"] > 70:
                        self.face_identified_success = True
                        # keep the result for a while
                        tmp_qeventloop = QEventLoop()
                        QTimer.singleShot(2000, tmp_qeventloop.quit)
                        tmp_qeventloop.exec_()
                        self.face_identified_success = False
                        break
                    else:
                        self.face_identified_success = False
            else:
                # delay
                tmp_qeventloop = QEventLoop()
                QTimer.singleShot(2000, tmp_qeventloop.quit)
                tmp_qeventloop.exec_()
            self.face_detect_timer.start()
            self.recognize_face_timer.start()
        else:
            self.face_identified_success = False

    def nfc_unlock(self):
        nfc_instance = Nfc("pn532_i2c:/dev/i2c-1")
        card_default_key = b'\x00' * 8
        card_blank_token = b'\xFF' * 1024 * 4

        for target in nfc_instance.poll():
            try:
                if target.auth(card_default_key if type(target) == Desfire else card_blank_token):
                    self.nfc_identified_success = True
                    time.sleep(2)
                    self.nfc_identified_success = False
            except TimeoutException:
                pass

    def authenticate_with_pin(self):
        self.pin_auth_window.show()

    def door_control(self):
        if self.face_identified_success or self.nfc_identified_success or self.pin_identified_success:
            self.door_unlocked = True
            # keep the result for a while
            tmp_qeventloop = QEventLoop()
            QTimer.singleShot(2000, tmp_qeventloop.quit)
            tmp_qeventloop.exec_()
            self.door_unlocked = False


class pin_auth_window(QMainWindow):
    def __init__(self, main_window):
        super(pin_auth_window, self).__init__()
        # setup interface from Designer
        self.ui = pinui.Ui_MainWindow()
        self.ui.setupUi(self)
        self.main_window = main_window
        self.ui.pushButton_2.clicked.connect(self.authenticate_with_pin)
        self.ui.Num_1.clicked.connect(lambda: self.ui.lineEdit.insert("1"))
        self.ui.Num_2.clicked.connect(lambda: self.ui.lineEdit.insert("2"))
        self.ui.Num_3.clicked.connect(lambda: self.ui.lineEdit.insert("3"))
        self.ui.Num_4.clicked.connect(lambda: self.ui.lineEdit.insert("4"))
        self.ui.Num_5.clicked.connect(lambda: self.ui.lineEdit.insert("5"))
        self.ui.Num_6.clicked.connect(lambda: self.ui.lineEdit.insert("6"))
        self.ui.Num_7.clicked.connect(lambda: self.ui.lineEdit.insert("7"))
        self.ui.Num_8.clicked.connect(lambda: self.ui.lineEdit.insert("8"))
        self.ui.Num_9.clicked.connect(lambda: self.ui.lineEdit.insert("9"))
        self.ui.Num_0.clicked.connect(lambda: self.ui.lineEdit.insert("0"))
        self.ui.pushButton_1.clicked.connect(lambda: self.ui.lineEdit.backspace())

    def authenticate_with_pin(self):
        if self.ui.lineEdit.text() == self.main_window.passcode:
            self.main_window.pin_identified_success = True
            # keep the result for a while
            tmp_qeventloop = QEventLoop()
            QTimer.singleShot(2000, tmp_qeventloop.quit)
            tmp_qeventloop.exec_()
            self.main_window.pin_identified_success = False
            

if __name__ == "__main__":
    # initialize argparse
    arg_parser = argparse.ArgumentParser(description="An intelligence door locker with face recognize feature.")
    arg_parser.add_argument(
        "--debug",
        help="Show debug messages.",
        action="store_true",
    )
    # parse arguments
    args = arg_parser.parse_args()
    
    mainApp = QApplication(sys.argv)
    mainWindow = main_window(args)
    sys.exit(mainApp.exec_())
